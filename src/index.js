﻿//Константы оформляются в особом стиле
const WIDTH = 10;
const HEIGHT = 10;
const TIMEFRAME = 500;

class Cell {
    constructor(element) {
        this.element = element;
    }

    // Определяем свойства через сеттеры и геттеры
    set hasSnake(value) {
        if (value) {
            // Строчка className может включать несколько классов, например "class1 class2".
            // Поэтому добавляем пробел.
            this.element.className += ' snake';
        } else {
            this.element.className = this.element.className.replace('snake', '');
        }
    }

    get hasSnake() {
        return this.element.className.indexOf('snake') > 0;
    }
	
	set hasFood (value) {
        if (value) {
            this.element.className += ' food';
        } else {
            this.element.className = this.element.className.replace('food', '');
        }
    }
	
	get hasFood () {
		return this.element.className.indexOf('food') > 0;
	}

    set step(value) {
        // Есть соглашение, что пользовательские атрибуты называются с префикса "data-".
        this.element.setAttribute('data-step', value);
    }

    get step() {
        // Явное преобразование строки к числу через объект-обертку.
        return new Number(this.element.getAttribute('data-step'));
    }
}

class Field {
    constructor(width, height) {
        this.width = width;
        this.height = height;
    }

    // В контексте классов методы определяются так
    getCell(x, y) {
        // Можно находить элементы DOM по id
        const cellElement = document.getElementById(y + '_' + x);
        return new Cell(cellElement);
    }

    getSnakeCells() {
        const result = [];
        // Можно находить все элементы DOM, относящиеся к некоторому классу
        for(const element of document.getElementsByClassName('snake')) {
            result.push(new Cell(element));
        }
        return result;
    }
}



class SnakeGame {
    constructor(field) {
        this.field = field;

        this.headX = 0;
        this.headY = 0;
        this.dx = 1;
        this.dy = 0;
        this.currentStep = 1;
		this.foodX = -10;
		this.foodY = -10;

        this.setSnakeHead();
    }

    getScore() {
        return 0;
    }

    playStep() {
        this.headX += this.dx;
        this.headY += this.dy;

        this.removeSnakeFromCells();
        this.setSnakeHead();

        this.currentStep++;
        return true;
    }

    setSnakeHead() {
        const headCell = this.field.getCell(this.headX, this.headY);
        headCell.hasSnake = true;
        headCell.step = this.currentStep;
    }
	
	setFood() {
        const foodCell = this.field.getCell(snakeGame.foodX, snakeGame.foodY);
        foodCell.hasFood = true;
        foodCell.step = this.makeFood();
	}

    removeSnakeFromCells() {
        for (const cell of this.field.getSnakeCells()) {
            cell.hasFood = false;
        }
    }
	
	removeFood() {
		if(snakeGame.headX === snakeGame.foodX && snakeGame.headY === snakeGame.foodY) {
			cell.hasFood = false;
		}
	}
	
	moveLeft () {
		if (snakeGame.dx !== -1 && snakeGame.dy !== 0) {
			snakeGame.dx = -1;
			snakeGame.dy = 0;
			snakeGame.setSnakeHead();
		}
	}
	
	moveRight () {
		if (snakeGame.dx !== 1 && snakeGame.dy !== 0) {
			snakeGame.dx = 1;
			snakeGame.dy = 0;			
			snakeGame.setSnakeHead();
		}
	}
	
	moveUp () {
		if (snakeGame.dx !== 0 && snakeGame.dy !== -1) {
			snakeGame.dx = 0;
			snakeGame.dy = -1;
			snakeGame.setSnakeHead();
		}
	}
	
	moveDown () {
		if (snakeGame.dx !== 0 && snakeGame.dy !== 1) {
			snakeGame.dx = 0;
			snakeGame.dy = 1;
			snakeGame.setSnakeHead();
		}
	}
	
	checkField() {
		if((snakeGame.headX === 10 && snakeGame.dx === 1)) {
			snakeGame.headX = -1;
		} else if (snakeGame.headY === 10 && snakeGame.dy === 1) {
			snakeGame.headY = -1;
		} else if(snakeGame.headY === -1 && snakeGame.dy === -1){
			snakeGame.headY = 10;
		} else if (snakeGame.headX === -1 && snakeGame.dx === -1) {
			snakeGame.headX = 10;
		}
	}
	
	foodOnArea(){
		if (snakeGame.foodX > 0 && snakeGame.foodY > 0)
			return true;
		else return false;
	}
	
	foodOnSnake() {
		if (snakeGame.headX === snakeGame.foodX && snakeGame.headY === snakeGame.foodY)
			return true;
		else return false;
	}
	/*
	нужно сделать проверку на змейку чтобы не попасть на змейку
	сделать проверку на еду
	
	
	*/
	makeFood() {
		if(this.foodOnArea && this.foodOnSnake) {
			
			var x = Math.floor(Math.random() * 100) % WIDTH;
			var y = Math.floor(Math.random() * 100) % HEIGHT;
			
			snakeGame.foodX = this.x;
			snakeGame.foodY = this.y;
		}
	}
}

// Храним игру в глобальной переменной, чтобы вызывать ее методы из обработчиков.
let snakeGame = null;

window.onload = () => {
    // Создаем игру после загрузки документа с полем.
    snakeGame = new SnakeGame(new Field(WIDTH, HEIGHT));

    // Главный цикл игры.
    let timer = setInterval(function () {
		snakeGame.checkField();
		snakeGame.makeFood();
		
        if (!snakeGame.playStep()) {
            // Игра закончена - останавливаем цикл.
			
			
            clearInterval(timer);
            alert('Game Over! Score: ' + snakeGame.getScore());
        }
    }, TIMEFRAME);
};

// Обработка действий пользователя.
document.onkeydown = e => {
    switch (e.keyCode) {
		case 65: 	//  65 : "a",
		case 37:	//  37 : "left arrow",
			snakeGame.moveLeft()
			break;
		case 39:	//  39 : "right arrow",
		case 68:	//  68 : "d",	
			snakeGame.moveRight()
			break;
		case 38:	//  38 : "up arrow",
		case 87:	//  87 : "w",
			snakeGame.moveUp()
			break;
		case 40:	//  40 : "down arrow",
		case 83:	//  83 : "s",
			snakeGame.moveDown()
			break;
    }
}